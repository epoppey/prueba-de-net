FROM mcr.microsoft.com/dotnet/aspnet:5.0-focal AS base
WORKDIR /app
ENV http_proxy=http://10.1.1.12:8080
ENV https_proxy=http://10.1.1.12:8080
ENV no_proxy=desasiatservicios.impuestos.gob.bo
RUN apt update -y
RUN apt install iputils-ping -y
RUN apt install curl -y
ENV ASPNETCORE_ENVIRONMENT="production"
ENV URL_AMBIENTE_SIAT="https://desasiatservicios.impuestos.gob.bo"
EXPOSE 80
RUN ln -snf /usr/share/zoneinfo/America/La_Paz /etc/localtime && echo $America/La_Paz > /etc/timezone

ENV ASPNETCORE_URLS=http://+:80
FROM mcr.microsoft.com/dotnet/sdk:5.0-focal AS build
WORKDIR /src
COPY ["prueba/prueba.csproj", "prueba/"]
ENV http_proxy=http://10.1.1.12:8080
ENV https_proxy=http://10.1.1.12:8080
ENV no_proxy=desasiatservicios.impuestos.gob.bo
RUN apt update -y
RUN apt install iputils-ping -y
RUN apt install curl -y
ENV ASPNETCORE_ENVIRONMENT="production"
ENV URL_AMBIENTE_SIAT="https://desasiatservicios.impuestos.gob.bo"

RUN ln -snf /usr/share/zoneinfo/America/La_Paz /etc/localtime && echo $America/La_Paz > /etc/timezone

RUN dotnet restore "prueba/prueba.csproj"
COPY . .
WORKDIR "/src/prueba"
RUN dotnet build "prueba.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "prueba.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "prueba.dll"]
