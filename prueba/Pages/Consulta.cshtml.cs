﻿using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;

namespace prueba.Pages
{
    public class ConsultaModel : PageModel
    {
        ///CONFIGURACION PARA RECUPERAR DEL SETTINGS
        private IConfiguration Configuration;
    
        public ConsultaModel(IConfiguration _configuration)
        {
            Configuration = _configuration;
        }
        ///FIN CONFIGURACION PARA RECUPERAR DEL SETTINGS

        public string Msg { get; set; }
        public string Mensaje { get; set; }

        public void OnPostRazonSocial(string txtNit)
        {
            ///CONFIGURACION PARA RECUPERAR DEL SETTINGS
            string connString = this.Configuration.GetConnectionString("ConexionOracleXSIN");

            string Nit = txtNit;

            if(!string.IsNullOrEmpty(Nit))
            {
                var queryString= string.Format("SELECT DENOMINACION_VC FROM VPDR_DATOS_CONTRIBUYENTES WHERE NIT_NB = " + Nit);

                using (OracleConnection connection = new OracleConnection(connString))
                {
                    try
                    {
                        connection.Open();
                        
                        OracleGlobalization info = connection.GetSessionInfo();
                        info.TimeZone = "America/La_Paz";
                        connection.SetSessionInfo(info);

                        Msg = "Conexión válida";
                        OracleCommand command = new OracleCommand(queryString, connection);
                        OracleDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Msg = reader.GetString(0);
                            }
                        }
                        else
                        {
                            Msg = "Sin resultados.";
                        }
                        reader.Close();
                    }
                    catch (Exception exception)
                    {
                        Msg = exception.Message;
                    }
                }
            }
            else
            {
                Msg = "Debe Ingresar un Nro de NIT...";
            }

            //Mensaje = Nit + " -- " + connString;

            Mensaje = Nit;

        }

        public void OnPostDireccion(string txtNit)
        {
            string Nit = txtNit;

            

            if(!string.IsNullOrEmpty(Nit))
            {
                var queryString= string.Format("SELECT DIRECCION_VC FROM VPDR_DATOS_CONTRIBUYENTES WHERE NIT_NB = " + Nit);

                using (OracleConnection connection = new OracleConnection("Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (COMMUNITY = tcp.world)(PROTOCOL = TCP)(Host = 10.1.7.236)(Port = 1551))(ADDRESS = (COMMUNITY = tcp.world)(PROTOCOL = TCP)(Host = 10.1.7.236)(Port = 1551)))(CONNECT_DATA =  (SID = sin)));User Id=DDS;Password=!xyz789;Persist Security Info=True;Min Pool Size=10;Connection Lifetime=120;Connection Timeout=60;Incr Pool Size=5;Decr Pool Size=2;"))
                {
                    try
                    {
                        connection.Open();
                        Msg = "Conexión válida";
                        OracleCommand command = new OracleCommand(queryString, connection);
                        OracleDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Msg = reader.GetString(0);
                                //return Msg;
                            }
                        }
                        else
                        {
                            Msg = "Sin resultados.";
                            //return Msg;
                        }
                        reader.Close();
                    }
                    catch (Exception exception)
                    {
                        Msg = exception.Message;
                        //return Msg;
                    }
                    //return Msg;
                }

            }
            else
            {
                Msg = "Debe Ingresar un Nro de NIT...";
            }

            Mensaje = Nit;

        }
        
        public void OnPostTelefono(string txtNit)
        {
            string Nit = txtNit;

            if(!string.IsNullOrEmpty(Nit))
            {
            
                var queryString= string.Format("SELECT TELEFONO_1_VC FROM VPDR_DATOS_CONTRIBUYENTES WHERE NIT_NB = " + Nit);

                using (OracleConnection connection = new OracleConnection("Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (COMMUNITY = tcp.world)(PROTOCOL = TCP)(Host = 10.1.7.236)(Port = 1551))(ADDRESS = (COMMUNITY = tcp.world)(PROTOCOL = TCP)(Host = 10.1.7.236)(Port = 1551)))(CONNECT_DATA =  (SID = sin)));User Id=DDS;Password=!xyz789;Persist Security Info=True;Min Pool Size=10;Connection Lifetime=120;Connection Timeout=60;Incr Pool Size=5;Decr Pool Size=2;"))
                {
                    try
                    {
                        connection.Open();
                        Msg = "Conexión válida";
                        OracleCommand command = new OracleCommand(queryString, connection);
                        OracleDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Msg = reader.GetString(0);
                                //return Msg;
                            }
                        }
                        else
                        {
                            Msg = "Sin resultados.";
                            //return Msg;
                        }
                        reader.Close();
                    }
                    catch (Exception exception)
                    {
                        Msg = exception.Message;
                        //return Msg;
                    }
                    //return Msg;
                }

            }
            else
            {
                Msg = "Debe Ingresar un Nro de NIT...";
            }

            Mensaje = Nit;
            
        }

    }
}
