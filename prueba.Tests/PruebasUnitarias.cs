using System;
using Xunit;
using prueba.Pages;
using prueba.Services;

using Microsoft.AspNetCore.Mvc.RazorPages;

namespace prueba.Tests
{
    public class PruebasUnitarias
    {
        /*
        [Fact]
        public void OnPostRazonSocial_Test()
        {
            //Given
            var paginaModelo = new ConsultaModel();
            //When
            //string resultado = paginaModelo.OnPostRazonSocial();
            //Then

        }
        */
        private readonly pruebaService _pruebaService;

        public PruebasUnitarias()
        {
            _pruebaService = new pruebaService();
        }

        #region Sample_TestCode
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(1)]
        public void IsPrime_ValuesLessThan2_ReturnFalse(int value)
        {
            var result = _pruebaService.IsPrime(value);

            Assert.False(result, $"{value} should not be prime");
        }
        #endregion


        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(5)]
        [InlineData(7)]
        public void IsPrime_PrimesLessThan10_ReturnTrue(int value)
        {
            var result = _pruebaService.IsPrime(value);

            Assert.True(result, $"{value} should be prime");
        }
/*
        [Theory]
        [InlineData(4)]
        [InlineData(6)]
        [InlineData(8)]
        [InlineData(9)]
        public void IsPrime_NonPrimesLessThan10_ReturnFalse(int value)
        {
            var result = _pruebaService.IsPrime(value);

            Assert.False(result, $"{value} should not be prime");
        }
        */

    }

}
